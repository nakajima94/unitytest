﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageClear : MonoBehaviour
{
    [SerializeField]
    private int StageNo;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    

    // Update is called once per frame
    void Update()
    {
        
    }
    void StageClearSave() {
        if (PlayerPrefs.GetInt("CLEAR", 0) < StageNo)
        {
            PlayerPrefs.SetInt("CLEAR", StageNo);
        }

    }


}
